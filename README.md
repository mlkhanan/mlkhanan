# Abdul Hanan[](https://gitlab.com/mlkhanan)
### Contact : mlk_hanan@hotmail.com
### Linkedin: https://inkedin.com/in/iamabdulhanan
### GitHub: https://github.com/mlkhanan

## Introduction
Passionate individuals are always motivated to learn and contribute back to society to make a difference. With a strong background in coding and project management, I bring a unique blend of technical expertise and leadership skills to the table. With extensive experience in web development using various technologies such as Full stack developer, MERN stack, and React Native, I am well-versed in both front-end and back-end development.

I am passionate about leveraging technology to drive innovation and create value for businesses and end-users alike. Whether developing a new web application, leading a team of developers, or managing a complex project, I always strive for excellence and seek to exceed expectations.

If you're looking for a skilled and adaptable professional with a proven track record of success in coding and project management, I would love to connect and explore opportunities to collaborate.

## Skills
- **Programming Languages:** PHP, JavaScript, TypeScript

- **Web Technologies:** PHP, JavaScript, Nodejs, Bootstrap, React Js, React Redux
- **Mobile Technologies:** React Native, Redux, Hooks
- **Frame Works:** OpenCart, Laravel, Laravel Lumen, CodeIgniter, Express.js
- **Web Server:** Xampp, Wamp, Mamp pro
- **Databases:** MySQL, MongoDB, MSSQL Server
- **Scripts:** JavaScript, TypeScript
- **Tools & IDEs:** Brackets, FileZilla, Dreamweaver, Visual Studio Code, WinSCP, Intellij idea, Web Storm, CLion, Workbench, Navicat, Azure
- **Markup Language:** HTML5
- **Operating System:** Windows, macOS

## Professional Experience:
#### Project: E- commerce and Job Portal 
#### Role: Backend Developer
#### Company: Capital Brand Tech 
#### Client: All4Truck Ltd
**Description:** All4truck.com is an e-commerce website developed specially to minimize the problems of the European transport sector.All4truck.com is the biggest trucking website and also for new and used commercial vehicles, packaging material, lubricants, toys, security equipment, wheels, tires and a lot more. Furthermore,this website includes everything forj obs.
#### Responsibilities:
- Develop the required functionalities
- Also worked with the frontend Developer team, testing team (QA)

**Environment & Technologies:** OpenCart, PHP 5.4+, JavaScript (jQuery, Ajax), Dreamweaver, FileZilla, Windows.

## Project: Student Portal
#### Role: Lead Developer
#### Company: BIMS
**Description:** University Management portal for Students and university faculty to serve multiple purposes such as handling students Complaints, Daily updates, student semester progress, Semester fee details, Courses enrolled, results etc. Automating multiple tasks easy but time consuming which can be done within a blink of an Eye. Reducing the burden of simple tasks on university faculty
#### Responsibilities:
- Develop the required functionalities 
- Managing the team
- Testing team (QA)

**Environment & Technologies:** Laravel, JavaScript (jQuery, Ajax), Web Strom, Windows.

## Project: MOTO-PRONO
#### Role: Full Stack Developer
#### Company: Get Ranked Pakistan
**Description:** Moto-prono an e commerce solution for multi sized businesses. A platform used for selling variety of goods which includes eatable items, garments etc. Allowing suppliers to manage their businesses efficiently with productivity with proper invoicing system and monthly earning etc. User friendly interface for the customers with the best goods available in the town.
#### Responsibilities:
- Develop the required functionalities
- Designing and developing APIs.
- Meeting both technical and consumer needs etc.

**Environment & Technologies:** CodeIgniter, JavaScript (jQuery, Ajax), Bootstrap, FileZilla, Visual Studio code, Windows.

## Project: Sawari
#### Role: React Native Developer 
#### Company: Sawari USA Description: Confidential
#### Responsibilities:
- Develop the required functionalities
- Meeting both technical and consumer needs etc.

**Environment & Technologies:** React Native, TypeScript, GitHub, Visual Studio Code, IntelliJ idea Windows/ Mac.

## Project: Aone brain
#### Role: React Native Developer
#### Company: Freelancer
**Description:** Aone brain is a company that delivers standardized education tailored to the Nigerian curriculum with an emphasis to mastery of skills and attainment of knowledge required to develop a person from state zero.
#### Responsibilities:
- Developed the required UI
- Integration with the API and required functionalities
- Meeting both technical and consumer needs etc.

**Environment & Technologies:** React Native, Git Hub Visual Studio code, Windows.

## Project: AG Dashboard v3.0
#### Role: Full Stack Developer
#### Company: Noor It services Rawalpindi
#### Client: Elite info System (EIS) Malaysia
**Description:** AG (Audit General) Dashboard. It is for the Malaysian GOVT parliament to manage each and every activity related to the state.
#### Responsibilities:
- Develop the required functionalities
- Enhanced the Graphs and Reporting Module.
- Bugs Fixes
- Enhanced Queries
- Meeting both technical and consumer needs etc.
- Also worked with the internal testing team (QA)
                
**Environment & Technologies:** Laravel, Bootstrap, Workbench, Git Lab Visual Studio code, Windows.

## Project: CMS
#### Role: Project manager, Tech Lead and Mobile App Developer
#### Company: Noor IT services Rawalpindi
**Description:** It is a management App. Can't disclose the features before its official release. 
#### Responsibilities:
- Requirements Analysis
- Created ER Diagram
- Created UX on adobe XD
- Developed the UI After complete Analysis.
- Breakdown of a project and Estimation of a project.
- Meeting both technical and consumer needs etc.
- Also worked with the testing team (QA)

**Environment & Technologies:** React Native, Adobe XD, GitHub, Visual Studio code, Windows.

## Project: POS Aviation
#### Role: Full Stack Developer
#### Company: Noor IT services Rawalpindi
#### Client: Elite info System (EIS) Malaysia
**Description:** Pos Aviation provides ground handling services to international & local handling airlines and among the services include Passenger Handling, Ramp Handling, Cargo Handling, In-Flight Catering & Aircraft Maintenance & Engineering and Integrated Logistic & Services.
#### Responsibilities:
- Analysis of existing Features.
- Enhancement of existing features.
- Bug fixes
- Breakdown and estimation of new features.
- database enhancements according to the new features
- Designing and developing new APIs.
- Meeting both technical and consumer needs etc.
- Also worked with the testing team (QA)

**Environment & Technologies:** Laravel Lumen, Bootstrap, JavaScript (jQuery, Ajax), MS Sql Server, Visual Studio code, Windows.

## Project: Digital Hire
#### Role: Mobile App Developer, React js Developer
#### Company: IqSoft Rawalpindi
**Description:** Digital hiring solution streamlines the recruitment process for companies by automating and optimizing the hiring process from job posting to candidate selection. Utilizing cutting-edge technology, it simplifies the process of finding the best-suited candidates for the job, increasing efficiency and saving time and resources.
#### Responsibilities:
- Analysis of existing Features.
- Development Modern/whole system from scratch.
- Breakdown and estimation of new System.
- Worked closely with QA and backend developer team.
- Meeting Customer Need.
- Development of new features

**Environment & Technologies:** React Native, React Js, React Redux, Visual Studio code, Windows/ macOS.

## Project: National Allergy Centre 
#### Role: Software Engineer            
#### Company: Tech Project, NIH Islamabad
**Description:** This Web portal is a comprehensive solution for automating / managing hospital. It streamlines the process for doctors and other medical staff by providing easy access to patient information, such as fees and lab test results, as well as managing the delivery of vaccines. This software simplifies the management of patient care, making it more efficient and effective.
#### Responsibilities:
- Requirement gathering.
- Full Stack developer.
- Breakdown and estimation of a project.
- Database enhancements according to the new features.
- Implementation / Staff training

**Environment & Technologies:** CodeIgniter, JavaScript (jQuery, Ajax), Visual Studio code, Windows.

## Project: National Fungal Disease Surveillance System
#### Role: Software Engineer
#### Company: Tech Project, NIH Islamabad
**Description:** National Fungal Disease Surveillance System is a web-based solution designed to monitor and track fungal infections across the country. It allows healthcare professionals and researchers to collect, analyze, and share data on fungal diseases in real-time. This system helps in identifying outbreaks, tracking trends, and monitoring the effectiveness of treatment. It enables healthcare providers to make informed decisions and improve patient outcomes. This software is a vital tool in the fight against fungal infections and improving public health.
#### Responsibilities:
- Analysis of existing Features.
- Enhancement of existing features.
- Bug fixes.
- Breakdown and estimation of new features.
- Database enhancements according to the new features.
- Designing and developing new APIs.
- Meeting both technical and consumer needs etc.
- Also worked with the testing team (QA).

**Environment & Technologies:** CodeIgniter, JavaScript (jQuery, Ajax), Visual Studio code, Windows/ macOS.

## Project: National Cancer Registry of Pakistan (Phase 1)
#### Role: Software Engineer
#### Company: Tech Project, NIH Islamabad
**Description:** National Cancer Registry of Pakistan is a web-based platform that provides a centralized database of cancer cases in the country. It allows healthcare professionals and researchers to collect, analyze, and share data on cancer cases in real-time. The system enables healthcare providers to make informed decisions and improve patient outcomes by tracking trends, identifying high-risk areas and monitoring the effectiveness of treatment.
#### Responsibilities:
- Requirement gathering.
- Full Stack developer.
- Database design
- Breakdown and estimation of a project.
- Implementation / Staff training

**Environment & Technologies:** CodeIgniter, JavaScript (jQuery, Ajax), Visual Studio code, Windows.
